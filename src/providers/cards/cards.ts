import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { Card } from '../../models/card';

@Injectable()
export class CardsApi {
    
  cardsApiUrl = "https://api.magicthegathering.io/v1/cards";
  constructor(public http: Http) { }

  // Load latest meals recipes

  load(): Observable<Card[]> {
    return this.http.get(`${this.cardsApiUrl}/`)
      .map(res => <Card[]>res.json().cards)      
  }
  
  loadDetails(id: string): Observable<Card[]> {
    return this.http.get(`${this.cardsApiUrl}/?id=`+id)
      .map(res => <Card[]>res.json().cards)      
  }
  
}