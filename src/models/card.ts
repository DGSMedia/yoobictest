export interface Card {
    name :string,
    number:string,
    rarity:string,
    artist:string,
    type:string,
    power: number,
    toughness:number,
    imageUrl: string,
    id: string,
    text: string
}