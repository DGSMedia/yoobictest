import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Card } from '../../models/card';
import {  CardsApi } from '../../providers/cards/cards';

@Component({
  selector: 'page-card',
  templateUrl: 'card.html',
})


export class CardPage {
  id: string;
  card: Card
  constructor(public navCtrl: NavController, private navParams: NavParams,private API: CardsApi) {
    this.id = navParams.get('id');    
    API.loadDetails(this.id).subscribe(res => {
      this.card = res[0];
    })
  }
}
