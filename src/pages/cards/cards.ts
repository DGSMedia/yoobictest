import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Card } from '../../models/card';

import {  CardsApi } from '../../providers/cards/cards';
import { CardPage } from '../card/card';


@IonicPage()
@Component({
  selector: 'page-cards',
  templateUrl: 'cards.html',
})


export class CardsPage {
  cards: Card[]
  
  constructor(public navCtrl: NavController, private API: CardsApi) {
    API.load().subscribe(res => {
      this.cards = res;
      console.log(this.cards);
    })
  }

  goToDetails(id: string) {
    this.navCtrl.push(CardPage, {id});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CardsPage');
  }


  

}
