import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { MealsPage } from '../meals/meals';
import { CardsPage } from '../cards/cards';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public nav: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login() {
    this.nav.setRoot(CardsPage);
    //this.nav.push(MealsPage);
    //this.nav.pop();
  }

}



